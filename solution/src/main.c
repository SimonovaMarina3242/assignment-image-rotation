#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    if (argc != 3)
    {
        printf("Check arguments!\n");
        return 1;
    }

    struct image in = construct_image();

    enum read_status read_result = img_from_bmp(argv[1], &in);

    if (read_result != READ_OK)
    {
        destroy_image(in);
        return 1;
    }

    struct image out = transform(in, rotate);

    enum write_status write_result = bmp_from_img(argv[2], out);

    if (write_result != WRITE_OK)
    {
        destroy_image(in);
        destroy_image(out);
        return 1;
    }

    destroy_image(in);
    destroy_image(out);

    return 0;
}
