#include "bmp.h"
#include "file_utils.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

#define BF_TYPE 0x4D42
#define BI_SIZE 40
#define BI_PLANES 1
#define BIT_COUNT 24

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t bmp_padding(uint64_t width)
{
    uint8_t padding = width % 4;
    return padding;
}

enum read_status from_bmp(FILE *file, struct image *img)
{
    struct bmp_header header = {0};

    // Read bmp header from file into struct
    size_t read_result = fread(&header, sizeof(header), 1, file);
    if (read_result != 1)
        return READ_INVALID_HEADER;

    img->width = header.biWidth;
    img->height = header.biHeight;

    // Set cursor position to pixel block
    fseek(file, header.bOffBits, SEEK_SET);

    img->data = malloc(header.biHeight * header.biWidth * sizeof(struct pixel));

    uint8_t pad_bytes = bmp_padding(header.biWidth);

    for (uint64_t row = 0; row < header.biHeight; row++)
    {
        read_result = fread(&(img->data)[pixel_address(0, row, header.biWidth)], sizeof(*img->data), header.biWidth, file);
        if (read_result != header.biWidth)
            return READ_INVALID_BITS;
        // Set cursor to the next pixel row
        if (pad_bytes != 0)
            fseek(file, pad_bytes, SEEK_CUR);
    }

    return READ_OK;
}

enum read_status img_from_bmp(char *name, struct image *img)
{
    FILE *file = {0};

    enum open_status open_result = file_open(name, "r", &file);

    if (open_result)
        return READ_ERROR;

    enum read_status read_result = from_bmp(file, img);

    enum close_status close_result = file_close(file);

    if (close_result)
        return READ_ERROR;

    return read_result;
}

enum write_status to_bmp(FILE *file, struct image const img)
{
    struct bmp_header header = {
        .bfType = BF_TYPE,
        .bfileSize = (bmp_padding(img.width) + img.width * sizeof(struct pixel)) * img.height + sizeof(struct bmp_header),
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BI_SIZE,
        .biWidth = img.width,
        .biHeight = img.height,
        .biPlanes = BI_PLANES,
        .biBitCount = BIT_COUNT,
        .biSizeImage = (bmp_padding(img.width) + img.width * sizeof(struct pixel)) * img.height};

    size_t write_result = fwrite(&header, sizeof(header), 1, file);
    if (write_result != 1)
        return WRITE_ERROR;

    // Set cursor position to pixel block
    fseek(file, header.bOffBits, SEEK_SET);

    uint8_t pad_bytes = bmp_padding(img.width);
    uint32_t pad = 0;

    for (uint64_t row = 0; row < img.height; row++)
    {
        write_result = fwrite(&img.data[pixel_address(0, row, header.biWidth)], sizeof(struct pixel), img.width, file);

        if (write_result != img.width)
            return WRITE_ERROR;

        if (pad_bytes != 0)
        {
            // Write padding before next pixel row
            write_result = fwrite(&pad, sizeof(uint8_t), pad_bytes, file);
            if (write_result != pad_bytes)
                return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

enum write_status bmp_from_img(char *name, struct image const img)
{
    FILE *file = {0};

    enum open_status open_result = file_open(name, "w+", &file);

    if (open_result)
        return WRITE_ERROR;

    enum write_status write_result = to_bmp(file, img);

    enum close_status close_result = file_close(file);

    if (close_result)
        return WRITE_ERROR;

    return write_result;
}
