#include "file_utils.h"
#include <stdio.h>

enum open_status file_open(char *name, char *mode, FILE **file)
{
    *file = fopen(name, mode);

    if (*file == NULL)
        return OPEN_ERROR;

    return OPEN_OK;
}

enum close_status file_close(FILE *file)
{
    int result = fclose(file);
    if (result == -1)
        return CLOSE_ERROR;

    return CLOSE_OK;
}
