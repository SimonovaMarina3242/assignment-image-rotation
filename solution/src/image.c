#include "image.h"

#include <stdlib.h>

uint64_t pixel_address(uint64_t x, uint64_t y, uint64_t image_width)
{
    uint64_t address = x + y * image_width;
    return address;
}

struct image construct_image()
{
    return (struct image) {0};
}

void destroy_image(struct image img)
{
    free(img.data);
}
