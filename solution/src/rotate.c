#include "image.h"
#include "rotate.h"

#include <stdint.h>
#include <stdlib.h>

void rotate(struct image in, struct image *out)
{
    out->width = in.height;
    out->height = in.width;

    for (uint64_t row = 0; row < in.height; row++)
    {
        for (uint64_t col = 0; col < in.width; col++)
        {
            out->data[pixel_address((out->width - 1 - row), col, out->width)] = in.data[pixel_address(col, row, in.width)];
        }
    }
}
