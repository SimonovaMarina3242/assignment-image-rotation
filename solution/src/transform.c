#include "image.h"
#include "transform.h"

#include <stdint.h>
#include <stdlib.h>

struct image transform(struct image in, void (*func)(struct image, struct image*))
{
    struct image img = construct_image();
    img.data = malloc(sizeof(struct pixel) * in.height * in.width);

    func(in, &img);
    return img;
}
