#ifndef HEADER_TRANSFORM
#define HEADER_TRANSFORM
#include "image.h"

struct image transform(struct image in, void (*func)(struct image, struct image*));

#endif
