#ifndef HEADER_IMAGE
#define HEADER_IMAGE
#include <stdint.h>

// Struct for the pixel
// 1 Byte for blue, green, red
#pragma pack(push, 1)
struct pixel
{
    uint8_t b, g, r;
};
#pragma pack(pop)

// Struct for the image
// Width, Height & info for pixel data
struct image
{
    uint64_t width, height;
    struct pixel *data;
};

uint64_t pixel_address(uint64_t x, uint64_t y, uint64_t image_width);

struct image construct_image();

void destroy_image(struct image img);

#endif
