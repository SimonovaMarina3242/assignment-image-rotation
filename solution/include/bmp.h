#ifndef HEADER_BMP
#define HEADER_BMP

#include "image.h"
#include <stdint.h>
#include <stdio.h>

// Padding calculation
uint8_t bmp_padding(uint64_t width);

// Status enum for reading
enum read_status
{
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
};

// Load from bmp without file open\close
enum read_status from_bmp(FILE *file, struct image *img);

// Load image from bmp, handling file open\close
enum read_status img_from_bmp(char *name, struct image *img);

// Status enum for writing
enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR
};

// Write to bmp without file open\close
enum write_status to_bmp(FILE *file, struct image const img);

// Save image to bmp, handling file open\close
enum write_status bmp_from_img(char *name, struct image const img);

#endif
