#ifndef HEADER_ROTATE
#define HEADER_ROTATE
#include "image.h"

void rotate(struct image in, struct image *out);

#endif
