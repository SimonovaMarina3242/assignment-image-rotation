#ifndef HEADER_FILE
#define HEADER_FILE

#include <stdio.h>

// Status enum for file open
enum open_status
{
    OPEN_OK = 0,
    OPEN_ERROR
};

// Open file with correct mode
enum open_status file_open(char *name, char *mode, FILE **file);

// Status enum for file close
enum close_status
{
    CLOSE_OK = 0,
    CLOSE_ERROR
};

// Close file
enum close_status file_close(FILE *file);

#endif
